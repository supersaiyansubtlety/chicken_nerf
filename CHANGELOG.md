- 1.2.1 (17 Dec. 2024): Marked as compatible with 1.21.4
- 1.2.0 (11 Nov. 2024): Updated for 1.21.2-1.21.3
- 1.1.1 (4 Sep. 2024): Marked as compatible with 1.21.1
- 1.1.0 (12 Jul. 2024):
  - Updated for 1.21!
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
  [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates (client-side)
- 1.0.22 (9 May 2024):
  - Marked as compatible with 1.20.6
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
  - Minor internal changes
- 1.0.21 (24 Apr. 2024): Marked as compatible with 1.20.5
- 1.0.20 (28 Jan. 2024): Marked as compatible with 1.20.3 and 1.20.4
- 1.0.19 (10 Nov. 2023): Updated for 1.20.2
- 1.0.18 (21 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.0.17 (31 Mar. 2023):
  - Updated for 1.19.3  and 1.19.4
  - Temporarily removed Better Animals Plus integration as it hasn't been updated
- 1.0.16 (16 Aug. 2022): Marked as compatible with 1.19.2
- 1.0.15 (29 Jul. 2022):
  - Marked as compatible with 1.19.1
  - Minor internal changes
- 1.0.14 (28 Jun. 2022): Updated for 1.19!
- 1.0.13 (29 May 2022): Marked as compatible with 1.18.2
- 1.0.12 (2 Feb. 2022):
  - Added integration with [BetterAnimalsPlus](https://www.curseforge.com/minecraft/mc-mods/betteranimalsplus)!
  - Geese, Pheasants and Turkeys are effected. 
  - Thanks to [itsmeow](https://github.com/itsmeow) for making this possible by adding events to BAP!
  - Cloth Config is now optional.
- 1.0.11 (27 Dec. 2021): Removed 'All changes require a restart' tooltips, because they don't.
- 1.0.10 (13 Dec. 2021): Added 'Enabled translation fetching' option
- 1.0.9 (12 Dec. 2021): Marked as compatible with 1.18.1
- 1.0.8 (8 Dec. 2021):
  - Marked as compatible with 1.18.
  - AutoConfig is no longer required, as it's built in to cloth config.
- 1.0.7 (9 Jul. 2021): Marked as compatible with 1.17.1
- 1.0.6 (13 Jun. 2021): 
  - Updated for 1.17
  - No longer bundles Cloth Config and Auto Config (updated), you must install these dependencies separately
- 1.0.5 (15 Jan. 2021): Translations are now handled through [CrowdinTranslate](https://crowdin.com/project/chicken-nerf). 
  Marked as compatible with 1.16.5. 
- 1.0.4-1 (4 Nov. 2020): Fixed grammar in configs (layed -> laid) and corrected default value for `averageChickensFromEgg` so that you average a little more than one chicken per breeding. 
- 1.0.4 (3 Nov. 2020): Marked as compatible with 1.16.4. 
- 1.0.3 (18 Sep. 2020): Fixed config for average number of chickens from an egg so that it now actually respects the config and uses the correct formula. 
  Marked as compatible with 1.16.3.
  Updated embedded dependencies. 